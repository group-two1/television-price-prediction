import numpy as np
from flask import Flask, render_template, request
import pickle
import pandas as pd
# from flask_navigation import Navigation
# from flask.ext.navigation import Navigation

app = Flask(__name__)
model = pickle.load(open("regr.pkl","rb"))

@app.route('/')
def home():
    return render_template('home.html')


@app.route('/predict',methods=['POST'])
def predict():
    brand =(request.form['brand'])
    resolution =(request.form['resolution'])
    size = int(request.form['size'])
    operatingSys =(request.form['os'])
    Rating = float(request.form['rating'])
    prediction = model.predict(
        pd.DataFrame({'Brand': brand, 'Resolution': resolution, 'Size ': size, 'Operating System': operatingSys, 'Rating': Rating},
        index=[0])

    ) 
   
    output = round(prediction[0], 2)
    return render_template('home.html', prediction_text='The Predicted Price Of The Television is Nu: {}'.format(output))

if __name__ == "__main__":
    app.run(debug=True ,port=5000,use_reloader=False)
